﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Antick.Contracts.Apps.QuotesApi;

namespace Antick.Contracts.Msgs.QuotesApi
{
    public class NewQuotes
    {
        /// <summary>
        /// Инструмент
        /// </summary>
        public string Instrument { get; set; }

        /// <summary>
        /// Таймфрейм.
        /// </summary>
        public string TimeFrame { get; set; }

        /// <summary>
        /// Полученные бары
        /// </summary>
        public List<Quote> Quotes { get; set; }

        /// <summary>
        /// Время создания сообщения
        /// </summary>
        public DateTime CreatedTime { get; set; }
    }
}
