﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Domain;

namespace Antick.Contracts.Msgs.RatesApi
{
    public class NewRates
    {
        public List<Rate> Rates { get; set; }

        public InstrumentComplex Instrument { get; set; }

        public TimeFrameComplex TimeFrame { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
