﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Antick.Contracts.Apps.SingalsProvider;
using Antick.Contracts.Domain;

namespace Antick.Contracts.Msgs.SignalsProvider
{
    public class SignalCreated
    {
        public Guid SignalId { get; set; }
        
        /// <summary>
        /// Имя поставщика сигнала
        /// </summary>
        public string ProviderName { get; set; }

        /// <summary>
        /// Тип инструмента 
        /// </summary>
        public Instrument Instrument { get; set; }

        /// <summary>
        /// Типа исполнения сигнала
        /// </summary>
        public SignalExecutionType ExecutionType { get; set; }


        /// <summary>
        /// Направление сигнала
        /// </summary>
        public SignalDirection Direction { get; set; }
        
        /// <summary>
        /// Последняя известная цена по инструменту, на момент публикации сигнала
        /// </summary>
        public double LastPrice { get; set; }

        /// <summary>
        /// Индекс последней закрытой котировки
        /// </summary>
        public long LastRateIndex { get; set; }

        public DateTime LastRateTime { get; set; }

        /// <summary>
        /// СтопЛосс
        /// </summary>
        public double StopLoss { get; set; }

        /// <summary>
        /// Тейкпрофит
        /// </summary>
        public double TakeProfit { get; set; }
    }
}
