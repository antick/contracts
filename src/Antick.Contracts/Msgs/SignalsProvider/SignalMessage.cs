﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Domain;

namespace Antick.Contracts.Msgs.SignalsProvider
{
    /// <summary>
    /// Общая информация по одному конкретному сигналу, или по поставщику сигнала в целом
    /// </summary>
    public class SignalMessage
    {
        /// <summary>
        /// общая тип сообщения(может указывать на тип сообщения)
        /// </summary>
        public string MessageType { get; set; }

        /// <summary>
        /// Гуид сигнала(если информация касается конкретно одного сигнала
        /// </summary>
        public Guid? SignalId { get; set; }
        
        /// <summary>
        /// Гуид поставщика сигналов
        /// </summary>
        public Guid ProviderId { get; set; }

        /// <summary>
        /// инструмент
        /// </summary>
        public InstrumentComplex Instrument { get; set; }

        /// <summary>
        /// Таймфрейм
        /// </summary>
        public TimeFrameComplex TimeFrame { get; set; }

        /// <summary>
        /// Последняя известная завершенная котировка
        /// </summary>
        public Rate LastRate { get; set; }

        /// <summary>
        /// Любая другая информация (json в string)
        /// </summary>
        public string CustomInformation { get; set; }
    }
}
