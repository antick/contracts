﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Antick.Contracts.Msgs.SignalsProvider
{
    public class SignalUpdated
    {
        public Guid SignalId { get; set; }
        
        /// <summary>
        /// Индекс последней закрытой котировки
        /// </summary>
        public long LastRateIndex { get; set; }

        /// <summary>
        /// СтопЛосс
        /// </summary>
        public double StopLoss { get; set; }

        /// <summary>
        /// Тейкпрофит
        /// </summary>
        public double TakeProfit { get; set; }
    }
}
