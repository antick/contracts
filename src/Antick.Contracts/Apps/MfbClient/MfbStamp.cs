﻿using System;
using Antick.Contracts.Common;
using Antick.Contracts.Domain;

namespace Antick.Contracts.Apps.MfbClient
{
    public class MfbStamp : ITimeble
    {
        public Instrument Instrument { get; set; }

        public DateTime Time { get; set; }

        public double ShortVolume { get; set; }

        public double LongVolume { get; set; }

        public int ShortPositions { get; set; }

        public int LongPositions { get; set; }
    }
}
