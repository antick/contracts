﻿using System;
using System.Collections.Generic;
using Antick.Contracts.Common;
using Antick.Contracts.Domain;

namespace Antick.Contracts.Apps.OandaClient
{
    public class OrderBook : ITimeble
    {
        public Instrument Instrument { get; set; }

        public TimeFrame TimeFrame { get; set; }

        public double Rate { get; set; }

        public DateTime Time { get; set; }

        public Dictionary<double,PricePoint> Points { get; set; }
    }
}
