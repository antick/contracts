﻿using System.Diagnostics.CodeAnalysis;

namespace Antick.Contracts.Apps.OandaClient
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class PricePoint
    {
        public double PositionsShort { get; set; }

        public double PositionsLong { get; set; }

        public double OrdersShort { get; set; }

        public double OrdersLong { get; set; }

        ////
        public double ps { get { return PositionsShort;} set { PositionsShort = value; } }
        public double pl { get { return PositionsLong; } set { PositionsLong = value; } }
        public double os { get { return OrdersShort;} set { OrdersShort = value; } }
        public double ol { get { return OrdersLong;} set { OrdersLong = value; } }
    }
}
