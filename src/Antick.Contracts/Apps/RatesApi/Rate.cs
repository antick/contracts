﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Antick.Contracts.Apps.QuotesApi;

namespace Antick.Contracts.Apps.RatesApi
{
    public class Rate
    {
        /// <summary>
        /// Время старта бара
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// Цена открытыя
        /// </summary>
        public double Open { get; set; }

        /// <summary>
        /// Максимальная цена 
        /// </summary>
        public double High { get; set; }

        /// <summary>
        /// Минимальная ценав
        /// </summary>
        public double Low { get; set; }

        /// <summary>
        /// Цена закрытия
        /// </summary>
        public double Close { get; set; }

        /// <summary>
        /// Индекс бара
        /// </summary>
        public long Index { get; set; }

        // Дополнительные поля.

        /// <summary>
        /// Фактическая цена закрытия(актуально для Renko)
        /// </summary>
        public double CloseFact { get; set; }
    }
}
