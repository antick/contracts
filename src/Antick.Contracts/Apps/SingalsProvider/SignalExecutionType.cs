﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Antick.Contracts.Apps.SingalsProvider
{
    /// <summary>
    /// Тип входа в рынок сигнала
    /// </summary>
    public enum SignalExecutionType
    {
        None = 0,

        /// <summary>
        ///  Исполнение по рынку
        /// </summary>
        Market = 1,

        /// <summary>
        /// Отложенный сигнал 
        /// </summary>
        Delay = 2,
    }
}
