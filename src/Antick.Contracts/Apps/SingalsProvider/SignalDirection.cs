﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Antick.Contracts.Apps.SingalsProvider
{
    /// <summary>
    /// Тип сигнала по направления входа
    /// </summary>
    public enum SignalDirection
    {
        /// <summary>
        /// Нет данных
        /// </summary>
        None = 0,

        /// <summary>
        /// На покупку
        /// </summary>
        Buy = 1,

        /// <summary>
        /// На продажу
        /// </summary>
        Sell = 2
    }
}
