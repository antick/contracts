﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Antick.Contracts.Apps.QuotesApi
{
    public class DayResponse
    {
        public List<Quote> Candles { get; set; }
        public DateTime? LastCompletedDay { get; set; }
        public bool IsApiReady { get; set; }
    }
}
