﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Antick.Contracts.Apps.QuotesApi
{
    public class LastQuoteResponse
    {
        public Quote Quote { get; set; }
        public bool IsApiReady { get; set; }
    }
}
