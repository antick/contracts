﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Antick.Contracts.Apps.QuotesApi
{
    public class QuotesResponse
    {
        public List<Quote> Quotes { get; set; }

        public bool IsApiReady { get; set; }
    }
}
