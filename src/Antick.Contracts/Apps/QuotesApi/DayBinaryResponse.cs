﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Antick.Contracts.Apps.QuotesApi
{
    public class DayBinaryResponse
    {
        /// <summary>
        /// Биннарные данные за указанный день
        /// </summary>
        public string BinaryData { get; set; }
        
        /// <summary>
        /// Дата последнего завершенного дня. 
        /// </summary>
        public DateTime? LastCompletedDay { get; set; }

        public bool IsApiReady { get; set; }
    }
}
