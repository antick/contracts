﻿using System;
using Antick.Contracts.Common;

namespace Antick.Contracts.Apps.QuotesApi
{
    [Serializable]
    public class Quote : ITimeble
    {
        public DateTime Time { get; set; }
        public double Open { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Close { get; set; }
        public long Index { get; set; }
    }
}
