﻿using Antick.Contracts.Domain;

namespace Antick.Contracts.Specs
{
    public class InstrumentSpecItem
    {
        /// <summary>
        /// Инструмент
        /// </summary>
        public Instrument Instrument { get; set; }
        
        /// <summary>
        /// Кол-во "мажорных" пунктов. для EUR_USD это 4
        /// </summary>
        public int Digits { get; set; }

        /// <summary>
        /// Формат вывода котировки, для мажоров это 4 знака
        /// </summary>
        public string Format { get; set; }

        /// <summary>
        /// (Oanda) Стандартный шаг цены в книге заказов оанды
        /// </summary>
        public double Step { get; set; }

        /// <summary>
        /// (Oanda) Шаг сетки по инструменту
        /// </summary>
        public double StepInterval { get; set; }
    }
}
