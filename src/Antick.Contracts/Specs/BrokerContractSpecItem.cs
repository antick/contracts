﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Antick.Contracts.Domain;

namespace Antick.Contracts.Specs
{
    public class BrokerContractSpecItem
    {
        /// <summary>
        /// Название инструмента
        /// </summary>
        public Instrument Instrument { get; set; }

        public int MajorPoints { get; set; }        

        /// <summary>
        /// Минимальный размер лота
        /// </summary>
        public double LotMin { get; set; }

        /// <summary>
        /// Максимальный размер лота
        /// </summary>
        public double LotMax { get; set; }

        /// <summary>
        /// Размер контракта в 1 лот, в единицах изменения согластно инструменту
        /// </summary>
        public double Contract { get; set; }
        
        public double MajorPointPrice()
        {
            return Math.Pow(0.1, MajorPoints);
        }
    }
}
