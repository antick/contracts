﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Antick.Contracts.Domain;

namespace Antick.Contracts.Specs
{
    public class BrokerContractSpec
    {
        public BrokerContractSpecItem Get(Instrument instrument)
        {
            switch (instrument)
            {
                case Instrument.EUR_USD:
                    return new BrokerContractSpecItem
                    {
                        Instrument = instrument,
                        MajorPoints = 4,
                        LotMin = 0.01,
                        LotMax = 100,
                        Contract = 100000
                    };

                case Instrument.AUD_CAD:
                case Instrument.AUD_CHF:
                case Instrument.AUD_JPY:
                case Instrument.AUD_NZD:
                case Instrument.AUD_USD:
                case Instrument.CAD_CHF:
                case Instrument.CAD_JPY:
                case Instrument.CHF_JPY:
                case Instrument.EUR_AUD:
                case Instrument.EUR_CAD:
                case Instrument.EUR_CHF:
                case Instrument.EUR_GBP:
                case Instrument.EUR_JPY:
                case Instrument.EUR_NOK:
                case Instrument.EUR_NZD:
                case Instrument.EUR_SEC:
                case Instrument.GBP_AUD:
                case Instrument.GBP_CAD:
                case Instrument.GBP_CHF:
                case Instrument.GBP_JPY:
                case Instrument.GBP_NZD:
                case Instrument.GBP_USD:
                case Instrument.NZD_CAD:
                case Instrument.NZD_JPY:
                case Instrument.NZD_USD:
                case Instrument.USD_CAD:
                case Instrument.USD_CHF:
                case Instrument.USD_CNH:
                case Instrument.USD_HKD:
                case Instrument.USD_JPY:
                case Instrument.USD_MXN:
                case Instrument.USD_NOK:
                case Instrument.USD_SEK:
                case Instrument.USD_SGD:
                case Instrument.USD_ZAR:
                    return new BrokerContractSpecItem
                    {
                        Instrument = instrument,
                        LotMin = 0.01,
                        LotMax = 100,
                        Contract = 100000
                    };

                case Instrument.XAG_USD:
                    return new BrokerContractSpecItem
                    {
                        Instrument = instrument,
                        LotMin = 0.01,
                        LotMax = 100,
                        Contract = 5000
                    };

                case Instrument.XAU_USD:
                    return new BrokerContractSpecItem
                    {
                        Instrument = instrument,
                        LotMin = 0.01,
                        LotMax = 100,
                        Contract = 100
                    };

                case Instrument.XPD_USD:
                    return new BrokerContractSpecItem
                    {
                        Instrument = instrument,
                        LotMin = 0.1,
                        LotMax = 100,
                        Contract = 100
                    };
                    
                case Instrument.XPT_USD:
                    return new BrokerContractSpecItem
                    {
                        Instrument = instrument,
                        LotMin = 0.1,
                        LotMax = 100,
                        Contract = 50
                    };
                    
                default: throw new Exception($"Broker Contract Specification for Instrument {instrument} is not implemented");
            }
        }
    }
}
