﻿using System;
using Antick.Contracts.Domain;

namespace Antick.Contracts.Specs
{
    public static class InstrumentSpec
    {
        public static InstrumentSpecItem Get(string instrument)
        {
            var instrumentEnum = (Instrument)Enum.Parse(typeof(Instrument), instrument);
            return Get(instrumentEnum);
        }

        public static InstrumentSpecItem Get(Instrument instrument)
        {
            switch (instrument)
            {
                case Instrument.EUR_USD: 
                case Instrument.GBP_USD:
                case Instrument.USD_CAD:
                case Instrument.AUD_USD:
                case Instrument.USD_CHF:
                case Instrument.EUR_GBP:
                case Instrument.NZD_USD:
                    return new InstrumentSpecItem
                    {
                        Instrument = instrument,
                        Digits = 4,
                        Format = "0.0000",
                        Step = 0.0005,
                        StepInterval = 0.0050,
                    };
                    
                case Instrument.EUR_JPY:
                case Instrument.GBP_JPY:
                case Instrument.AUD_JPY:
                case Instrument.USD_JPY:
                    return new InstrumentSpecItem
                    {
                        Instrument = instrument,
                        Digits = 2,
                        Format = "0.00",
                        Step = 0.05,
                        StepInterval = 0.50,
                    };

                case Instrument.XAG_USD:
                    return new InstrumentSpecItem
                    {
                        Format = "0.0",
                        Step = 1,
                        StepInterval = 10.00,
                        Digits = 1
                    };

                case Instrument.XAU_USD:
                    return new InstrumentSpecItem
                    {
                        Format = "0.0",
                        Step = 0.5,
                        StepInterval = 5.00,
                        Digits = 1
                    };

                default: throw new Exception(string.Format("Specification for Instrument {0} is not implemented", instrument));
            }
        }
    }
}
