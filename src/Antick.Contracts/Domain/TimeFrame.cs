﻿

// ReSharper disable InconsistentNaming

namespace Antick.Contracts.Domain
{
    public enum TimeFrame
    {
        M1 = 6,
        M2 = 7,
        M3 = 8,
        M4 = 9,
        M5 = 5,
        M10 = 10,
        M15 = 11,
        M20 = 1,
        M30 = 12,
        H1 = 13,
        H4 = 14,
        D1 = 2,
        W1 = 3,
        MN1 = 4,
    }
}
