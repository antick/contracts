﻿using System;

namespace Antick.Contracts.Domain
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class CandleLight
    {
        public DateTime Time { get; set; }
        public double Open { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Close { get; set; }
    }
}
