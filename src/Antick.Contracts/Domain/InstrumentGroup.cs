﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Antick.Contracts.Domain
{
    /// <summary>
    /// Группа инструмента: Форекс, Фьючерс и так далее
    /// </summary>
    public enum InstrumentGroup
    {
        None = 0,
        Fx = 1,
        Futures = 2
    }
}
