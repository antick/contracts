﻿

// ReSharper disable InconsistentNaming

namespace Antick.Contracts.Domain
{
    public enum Instrument
    {
        None = 0,

        GBP_USD = 1,
        EUR_USD = 2,
        AUD_USD = 3,
        NZD_USD = 4,

        // Противники
        USD_CAD = 5,
        USD_CHF = 6,
        USD_JPY = 7,

        // Кроссы с еной
        EUR_JPY = 8,
        GBP_JPY = 9,
        AUD_JPY = 10,

        // Металл
        XAU_USD = 11,
        XAG_USD = 12,

        EUR_AUD = 13,
        EUR_CHF = 14,
        EUR_GBP = 15,
        GBP_CHF = 16,

        AUD_CAD = 17,
        AUD_NZD = 18,
        CAD_JPY = 19,
        EUR_CAD = 20,
        EUR_NOK = 21,
        EUR_NZD = 22,
        EUR_PLN = 23,
        EUR_SEC = 24,
        EUR_TRY = 25,
        GBP_CAD = 26,
        NZD_CAD = 27,
        NZD_JPY = 28,
        USD_CZK = 29,
        USD_HKD = 30,
        USD_MXN = 31,
        USD_NOK = 32,
        USD_PLN = 33,
        USD_SEK = 34,
        USD_SGD = 35,
        USD_THP = 36,
        USD_ZAR = 37,
        CHF_JPY = 38,
        AUD_CHF = 39,
        GBP_NZD = 40,
     
        CAD_CHF = 41,
        GBP_AUD = 42,
        SPA35 = 43,
        SGD_JPY = 44,
        GBP_NOK = 45,
        US30 = 46,
        EUR_ZAR =47,
        AUD_SGD = 48,
        GBP_SEK = 49,
        CHF_SGD = 50,
        EUR_SGD = 51,
        GBP_SGD = 52,
        COPPER = 53,
        COFFEE = 54,
        US500 = 55,
        COCOA = 56,
        UK100 = 57,
        COTTON = 58,
        NOK_JPY = 59,
        ZAR_JPY = 60,
        GBP_TRY = 61,
        USD_RUB = 62,
        SEK_JPY = 63,
        XAU_AUD = 64,
        XAU_EUR = 65,
        NAS100 = 66,
        US2000 = 67,
        FRA40 = 68,
        GER30 = 69,
        AUD200 = 70,
        JPN225 = 71,
        NOK_SEK = 72,
        USDX = 73,
        XAG_EUR = 74,
        XPT_USD = 75,
        XPD_USD = 76,
        USD_CNH = 77,
        IT40 = 78,
        XTI_USD = 79,
        XBR_USD = 80,
        NK50 = 81,
        XNG_USD = 82,
        CH50 = 83
    }
}
