﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Antick.Contracts.Domain
{
    /// <summary>
    /// Идентификатор поставщика инструмента, явно указывает откуда идет. Например из Оанды
    /// </summary>
    public enum InstrumentProvider
    {
        None = 0,
        Oanda = 1,
    }
}
