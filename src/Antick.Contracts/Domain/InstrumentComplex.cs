﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Antick.Contracts.Domain
{
    /// <summary>
    /// Полное определение инструмента
    /// </summary>
    public class InstrumentComplex
    {
        /// <summary>
        /// Имя иструмента, вида: EUR_USD
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Группа инструмента, вида Fx, 
        /// </summary>
        public InstrumentGroup Group { get; set; }

        /// <summary>
        /// Идентификатор поставщика данных
        /// </summary>
        public InstrumentProvider Provider { get; set; }


        public override string ToString()
        {
            return $"{Name};{Group};{Provider}";
        }
    }
}
