﻿namespace Antick.Contracts.Domain
{
    public class TimeFrameComplex
    {
        /// <summary>
        /// Тип тайфрейма
        /// </summary>
        public TimeFrameType Type { get; set; }

        /// <summary>
        /// Его значение
        /// </summary>
        public string Value { get; set; }

        public override string ToString()
        {
            return $"{Type};{Value}";
        }
    }
}
