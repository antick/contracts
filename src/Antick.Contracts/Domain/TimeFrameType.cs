﻿namespace Antick.Contracts.Domain
{
    public enum TimeFrameType
    {
        None = 0,

        /// <summary>
        /// Классический временной таймфрейм, описывает состояние в 1M, 1H и тд
        /// </summary>
        Time = 1,

        /// <summary>
        /// Renko бар - изменение только в рамках заданного хода, к времени не привязан, хвосты (HL) вне обычного хода не рисует.
        /// Точность 0 - фактически это не классические ренко, это самодельные неточные
        /// </summary>
        Renko = 2,

        /// <summary>
        /// Range бар - изменение только в рамках заданного хода, к времени не привязан, рисует хвосты (HL) так реализован как 
        /// на использование псевдотиков
        /// </summary>
        Range = 3,

        /// <summary>
        /// Renko бар - изменение только в рамках заданного хода, к времени не привязан, хвосты (HL) вне обычного хода не рисует.
        /// Точность размер 1 шага - наиболее близок к классическим ренко.
        /// </summary>
        RenkoV2 = 4,
    }
}
