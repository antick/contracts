﻿using System;
using Antick.Contracts.Common;

namespace Antick.Contracts.Domain
{
    [Serializable]
    public class Candle : ITimeble
    {
        public DateTime Time { get; set; }
        public double Open { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Close { get; set; }
        public int Volume { get; set; }
        public bool Complete { get; set; }
        public long Index { get; set; }
    }
}
