﻿using System;

namespace Antick.Contracts.Common
{
    /// <summary>
    /// Объект описывает некоторое состояние на момент указанного времени
    /// </summary>
    public interface ITimeble
    {
        DateTime Time { get; set; }
    }
}
